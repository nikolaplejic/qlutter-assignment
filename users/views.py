from flask import request, jsonify, session
from model import *

def add_user():
  username = request.form['username'].encode('utf-8')
  password = request.form['password'].encode('utf-8')
  user_id = create_user(username=username, password=password)
  user = get_user_by_id(user_id)
  return jsonify(user), 200

def delete_user(user_id):
  delete_user_by_id(user_id)
  return "", 200

def login():
  username = request.form['username'].encode('utf-8')
  password = request.form['password'].encode('utf-8')
  if check_password(username, password):
    user = get_user_by_username(username)
    session['logged_in'] = True
    session['username'] = username
    session['user_id'] = user['id']
    return "", 200

  return "", 401

def logout():
  if session and session['logged_in'] is True:
    del session['username']
    del session['user_id']
    session['logged_in'] = False
    return "", 200

  return "", 403

def user_info():
  if session and session['logged_in']:
    return jsonify({'username': session['username'], 'user_id':
                    session['user_id']})

  return "", 404
