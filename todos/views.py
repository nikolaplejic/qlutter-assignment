import datetime
import model

from flask import request, session, jsonify
from users.helpers import require_valid_user

@require_valid_user
def mark_todo_complete(todo_id):
  todo = model.get_todo_by_id(todo_id)

  if not todo or todo['user_id'] != session['user_id']:
    return "", 404

  now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
  model.update_todo(todo_id, is_completed=True, date_completed=now)
  return "", 200

@require_valid_user
def create_todo():
  todo = {
    'contents': request.form['contents'].encode('utf-8'),
    'user_id': session['user_id'],
  }
  todo_id = model.create_todo(**todo)
  todo = model.get_todo_by_id(todo_id)
  return jsonify(todo), 200

@require_valid_user
def todo(todo_id):
  todo = model.get_todo_by_id(todo_id)

  if not todo or todo['user_id'] != session['user_id']:
    return "", 404

  if request.method == 'DELETE':
    model.delete_todo_by_id(todo_id)
    return "", 200
  elif request.method == 'GET':
    return jsonify(model.get_todo_by_id(todo_id)), 200

  return "", 405

@require_valid_user
def list_todos():
  rule = request.url_rule.rule

  todo_items = []
  filters = {'user_id': session['user_id']}
  if 'incomplete' in rule:
    filters['is_completed'] = 0
  elif 'complete' in rule:
    filters['is_completed'] = 1

  todo_items = model.get_todos(**filters)
  return jsonify({'items': todo_items}), 200
