# ToDo App

## Instructions

* `virtualenv --no-site-packages .`
* `. bin/activate`
* `pip install -r requirements.txt`
* `python db.py` (sets up the database schema)
* `python app.py`

## Testing

To run the tests, just run `python tests.py`. N.B. that the app should be
running on `localhost:5000` before you do that.
