CREATE TABLE users (
  id INTEGER PRIMARY KEY,
  username VARCHAR(255),
  password VARCHAR(255)
);

CREATE TABLE todo_items(
  id INTEGER PRIMARY KEY,
  user_id INTEGER NULL,
  contents TEXT,
  is_completed BOOLEAN,
  date_completed DATETIME NULL,
  FOREIGN KEY(user_id) REFERENCES users(id)
);
