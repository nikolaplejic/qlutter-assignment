from db import get_db, query_db

def create_todo(**kwargs):
  db = get_db()
  cursor = db.cursor()

  query = """
    INSERT INTO todo_items
    (user_id, contents, is_completed, date_completed)
    VALUES
    (?, ?, ?, ?)
  """

  values = [
    kwargs.pop('user_id', None),
    kwargs.pop('contents', None),
    kwargs.pop('is_completed', False),
    kwargs.pop('date_completed', None),
  ]

  if values[0] is None or values[1] is None:
    raise AttributeError("Neither the user_id nor the contents cannot be blank.")
  cursor.execute(query, values)
  db.commit()
  return cursor.lastrowid

def get_todo_by_id(id):
  query = "SELECT * FROM todo_items WHERE id=?"
  return query_db(query, (id,), one=True)

def update_todo(id, **kwargs):
  db = get_db()
  cursor = db.cursor()

  query = "UPDATE todo_items SET {} WHERE id=?"

  values = []
  set_clauses = []
  for column in kwargs.keys():
    set_clauses.append(" {}=? ".format(column))
    values.append(kwargs.pop(column))

  values.append(id)
  cursor.execute(query.format(','.join(set_clauses)), values)
  return db.commit()

def delete_todo_by_id(id):
  db = get_db()
  cursor = db.cursor()

  query = "DELETE FROM todo_items WHERE id=?"
  cursor.execute(query, (id,))
  return db.commit()

def get_todos(**kwargs):
  query = "SELECT * FROM todo_items"

  values = []
  where_clauses = []
  for column in kwargs.keys():
    where_clauses.append(" {}=? ".format(column))
    values.append(kwargs.pop(column))

  if len(values) > 0:
    query += " WHERE {}".format(' AND '.join(where_clauses))

  return query_db(query, values)
