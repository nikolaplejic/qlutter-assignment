from db import get_db, query_db
from flask import request, jsonify, session

def require_valid_user(func):
  def func_wrapper(*args, **kwargs):
    if not session or session['logged_in'] is not True:
      return "", 403
    return func(*args, **kwargs)
  return func_wrapper
