import bcrypt

from db import get_db, query_db

def create_user(**kwargs):
  db = get_db()
  cursor = db.cursor()

  username = kwargs.pop('username', None)
  password = kwargs.pop('password', None)

  if username is None or password is None:
    raise AttributeError("Neither the username nor the password cannot be blank.")

  hashed_password = bcrypt.hashpw(password, bcrypt.gensalt())

  query = "INSERT INTO users (username, password) VALUES (?, ?)"
  cursor.execute(query, (username, hashed_password,))
  db.commit()
  return cursor.lastrowid

def get_user_by_username(username):
  query = "SELECT id, username, password FROM users WHERE username=?"
  return query_db(query, (username,), one=True)

def get_user_by_id(id):
  query = "SELECT id, username, password FROM users WHERE id=?"
  return query_db(query, (id,), one=True)

def delete_user_by_id(id):
  db = get_db()
  cursor = db.cursor()

  query = "DELETE FROM users WHERE id=?"
  cursor.execute(query, (id,))
  return db.commit()

def check_password(username, password):
  user = get_user_by_username(username)
  return bcrypt.hashpw(password, user['password'].encode('utf-8')) == user['password']
