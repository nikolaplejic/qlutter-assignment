import sqlite3
import settings

from flask import Flask, g
app = Flask(__name__)
app.config.from_object('settings.default')

def create_app():
  from users import users
  from todos import todos

  app.register_blueprint(users, url_prefix='/users')
  app.register_blueprint(todos)

  return app

if __name__ == '__main__':
  create_app().run()
