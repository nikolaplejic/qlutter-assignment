import os
import unittest
import tempfile
import datetime
import requests
import json

import db
from app import create_app

app = create_app()

class TodoTestCase(unittest.TestCase):
  def setUp(self):
    self.db_fd, app.config['DATABASE'] = tempfile.mkstemp()
    app.config['TESTING'] = True
    self.app = app.test_client()
    self.app_context = app.app_context()
    self.app_context.push()
    db.init_db()

  def tearDown(self):
    os.close(self.db_fd)
    os.unlink(app.config['DATABASE'])
    self.app_context.pop()

  def test_create_user(self):
    import bcrypt
    from users.model import create_user, get_user_by_username, check_password

    create_user(username='user', password='hunter22')
    user = get_user_by_username('user')

    assert user is not None
    assert user['username'] == 'user'
    assert check_password('user', 'hunter22') is True

  def test_create_todo(self):
    from todos.model import create_todo, get_todo_by_id
    create_todo(contents='YOLO', user_id=1)
    todo = get_todo_by_id(1)
    assert todo['contents'] == "YOLO"
    assert todo['is_completed'] == False
    assert todo['user_id'] == 1

  def test_delete_todo(self):
    from todos.model import create_todo, delete_todo_by_id, get_todo_by_id
    create_todo(contents='YOLO', user_id=1)

    todo = get_todo_by_id(1)
    assert todo is not None

    delete_todo_by_id(1)
    todo = get_todo_by_id(1)
    assert todo is None

  def test_update_todo(self):
    from todos.model import create_todo, update_todo, get_todo_by_id
    create_todo(contents='YOLO', user_id=1)

    todo = get_todo_by_id(1)
    assert todo['contents'] == "YOLO"
    assert todo['is_completed'] == False

    update_todo(1, contents="booya")

    todo = get_todo_by_id(1)
    assert todo['contents'] == "booya"
    assert todo['is_completed'] == False

    now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    update_todo(1, is_completed=True, date_completed=now)

    todo = get_todo_by_id(1)
    assert todo['contents'] == "booya"
    assert todo['is_completed'] == True

class ApiTestCase(unittest.TestCase):
  def setUp(self):
    """
    prepares a user we'll subsequently use for testing. makes sure creating
    users works.
    """
    r = requests.post("http://localhost:5000/users/",
                      data={'username': 'nikola', 'password': u'hunter22'})
    response = json.loads(r.content)
    assert r.status_code == 200
    assert response['username'] == 'nikola'
    self.created_user_id = response['id']

  def tearDown(self):
    """
    deletes the user created in the setUp method to keep the DB clean.
    """
    r = requests.delete("http://localhost:5000/users/{}/".format(self.created_user_id))
    assert r.status_code == 200

  def _log_in(self, **kwargs):
    username = kwargs.pop('username', 'nikola')
    password = kwargs.pop('password', 'hunter22')
    log_in_req = requests.post("http://localhost:5000/users/login/",
                               data={'username': username, 'password': password})
    assert log_in_req.status_code == 200
    cookies = log_in_req.cookies.get_dict()
    return cookies

  def test_login(self):
    """
    tests whether we can log in with the user created in the setUp method, and
    whether we handle invalid logins well.
    """
    log_in_req = requests.post("http://localhost:5000/users/login/",
                               data={'username': 'nikola', 'password': u'hunter22'})
    assert log_in_req.status_code == 200

    invalid_login_req = requests.post(
      "http://localhost:5000/users/login/",
      data={'username': 'nikola', 'password': u'obviously wrong'}
    )
    assert invalid_login_req.status_code == 401

  def test_active_user(self):
    """
    tests whether we can access a sample "protected" endpoint when logged in.
    additionally, tests how we handle access to protected endpoints when not
    logged in.
    """
    cookies = self._log_in()

    user_info_req = requests.get("http://localhost:5000/users/user_info/",
                                 cookies=dict(session=cookies['session']))
    assert user_info_req.status_code == 200

    logged_out_req = requests.get("http://localhost:5000/users/user_info/",
                                  cookies=dict(session=""))
    assert logged_out_req.status_code == 404

  def test_logout(self):
    """
    tests whether the logout procedure works, and whether it works properly for
    non-logged in users.
    """
    cookies = self._log_in()

    logout_req = requests.get("http://localhost:5000/users/logout/",
                             cookies=dict(session=cookies['session']))
    assert logout_req.status_code == 200
    new_cookies = logout_req.cookies.get_dict()

    logged_out_req = requests.get("http://localhost:5000/users/logout/",
                                  cookies=dict(session=new_cookies['session']))
    assert logged_out_req.status_code == 403

  def test_create_and_delete_todo(self):
    """
    - test whether a logged out user gets and error when trying to create todos;
    - test whether a logged in user can create todos normally;
    - test whether a logged in user can create delete normally;
    - test whether we get a 404 if trying to delete a non-existing todo item
    """
    logged_out_req = requests.post("http://localhost:5000/todo/",
                                   data={'contents': 'take over the world'},
                                   cookies={})
    assert logged_out_req.status_code == 403

    cookies = self._log_in()

    add_todo_req = requests.post("http://localhost:5000/todo/",
                                 data={'contents': 'take over the world'},
                                 cookies=dict(session=cookies['session']))
    assert add_todo_req.status_code == 200
    response = json.loads(add_todo_req.content)

    delete_todo_req = requests.delete(
      "http://localhost:5000/todo/{}/".format(response['id']),
      cookies=dict(session=cookies['session'])
    )
    assert delete_todo_req.status_code == 200

    invalid_todo_delete_req = requests.delete("http://localhost:5000/todo/666/",
                                              cookies=dict(session=cookies['session']))
    assert invalid_todo_delete_req.status_code == 404

  def test_mark_todo_as_done(self):
    """
    tests marking a todo item as done
    """
    cookies = self._log_in()

    add_todo_req = requests.post("http://localhost:5000/todo/",
                                 data={'contents': 'take over the world'},
                                 cookies=dict(session=cookies['session']))
    assert add_todo_req.status_code == 200
    response = json.loads(add_todo_req.content)

    mark_as_done_req = requests.post(
      "http://localhost:5000/todo/{}/complete/".format(response['id']),
      cookies=dict(session=cookies['session'])
    )
    assert mark_as_done_req.status_code == 200

    get_todo_req = requests.get(
      "http://localhost:5000/todo/{}/".format(response['id']),
      cookies=dict(session=cookies['session'])
    )
    assert get_todo_req.status_code == 200
    response = json.loads(get_todo_req.content)
    assert response['is_completed'] == 1

    delete_todo_req = requests.delete(
      "http://localhost:5000/todo/{}/".format(response['id']),
      cookies=dict(session=cookies['session'])
    )
    assert delete_todo_req.status_code == 200

  def test_get_todos(self):
    """
    tests fetching and filtering todo lists
    """
    cookies = self._log_in()

    # add an incomplete todo, ensure filtering works

    add_todo_req = requests.post("http://localhost:5000/todo/",
                                 data={'contents': 'take over the world'},
                                 cookies=dict(session=cookies['session']))
    todo = json.loads(add_todo_req.content)

    get_todos_req = requests.get("http://localhost:5000/todos/",
                                 cookies=dict(session=cookies['session']))
    assert get_todos_req.status_code == 200
    response = json.loads(get_todos_req.content)
    assert len(response['items']) == 1

    get_complete_todos_req = requests.get("http://localhost:5000/todos/complete/",
                                          cookies=dict(session=cookies['session']))
    assert get_complete_todos_req.status_code == 200
    response = json.loads(get_complete_todos_req.content)
    assert len(response['items']) == 0

    get_incomplete_todos_req = requests.get("http://localhost:5000/todos/incomplete/",
                                            cookies=dict(session=cookies['session']))
    assert get_incomplete_todos_req.status_code == 200
    response = json.loads(get_incomplete_todos_req.content)
    assert len(response['items']) == 1

    # mark the added todo item as complete, review the filtering

    mark_as_done_req = requests.post(
      "http://localhost:5000/todo/{}/complete/".format(todo['id']),
      cookies=dict(session=cookies['session'])
    )

    get_complete_todos_req = requests.get("http://localhost:5000/todos/complete/",
                                          cookies=dict(session=cookies['session']))
    assert get_complete_todos_req.status_code == 200
    response = json.loads(get_complete_todos_req.content)
    assert len(response['items']) == 1

    get_incomplete_todos_req = requests.get("http://localhost:5000/todos/incomplete/",
                                            cookies=dict(session=cookies['session']))
    assert get_incomplete_todos_req.status_code == 200
    response = json.loads(get_incomplete_todos_req.content)
    assert len(response['items']) == 0

    # delete the added todo item for good measure

    delete_todo_req = requests.delete(
      "http://localhost:5000/todo/{}/".format(todo['id']),
      cookies=dict(session=cookies['session'])
    )

  def test_get_multiple_todos(self):
    """
    tests fetching and filtering todo lists on a sample of 10 todo items.
    """
    cookies = self._log_in()

    todos = []
    for x in range(1,11):
      add_todo_req = requests.post("http://localhost:5000/todo/",
                                   data={'contents': 'take over the world {}'.format(x)},
                                   cookies=dict(session=cookies['session']))
      todo = json.loads(add_todo_req.content)
      todos.append(todo)

    get_todos_req = requests.get("http://localhost:5000/todos/",
                                 cookies=dict(session=cookies['session']))
    assert get_todos_req.status_code == 200
    response = json.loads(get_todos_req.content)
    assert len(response['items']) == 10

    get_complete_todos_req = requests.get("http://localhost:5000/todos/complete/",
                                          cookies=dict(session=cookies['session']))
    assert get_complete_todos_req.status_code == 200
    response = json.loads(get_complete_todos_req.content)
    assert len(response['items']) == 0

    get_incomplete_todos_req = requests.get("http://localhost:5000/todos/incomplete/",
                                            cookies=dict(session=cookies['session']))
    assert get_incomplete_todos_req.status_code == 200
    response = json.loads(get_incomplete_todos_req.content)
    assert len(response['items']) == 10

    for x in range(2,6):
      mark_as_done_req = requests.post(
        "http://localhost:5000/todo/{}/complete/".format(todos[x]['id']),
        cookies=dict(session=cookies['session'])
      )

    get_todos_req = requests.get("http://localhost:5000/todos/",
                                 cookies=dict(session=cookies['session']))
    assert get_todos_req.status_code == 200
    response = json.loads(get_todos_req.content)
    assert len(response['items']) == 10

    get_complete_todos_req = requests.get("http://localhost:5000/todos/complete/",
                                          cookies=dict(session=cookies['session']))
    assert get_complete_todos_req.status_code == 200
    response = json.loads(get_complete_todos_req.content)
    assert len(response['items']) == 4

    get_incomplete_todos_req = requests.get("http://localhost:5000/todos/incomplete/",
                                            cookies=dict(session=cookies['session']))
    assert get_incomplete_todos_req.status_code == 200
    response = json.loads(get_incomplete_todos_req.content)
    assert len(response['items']) == 6

    for todo in todos:
      delete_todo_req = requests.delete(
        "http://localhost:5000/todo/{}/".format(todo['id']),
        cookies=dict(session=cookies['session'])
      )

  def test_multiple_users(self):
    user1_cookies = self._log_in()

    create_user2 = requests.post("http://localhost:5000/users/",
                                 data={'username': 'joe', 'password': u'god'})
    response = json.loads(create_user2.content)
    user2_cookies = self._log_in(username='joe', password='god')

    nikolas_todos = []
    joes_todos = []

    # nikola's todos

    for c in ['a todo by nikola', 'another todo by nikola']:
      add_todo_req = requests.post("http://localhost:5000/todo/",
                                   data={'contents': c},
                                   cookies=dict(session=user1_cookies['session']))
      response = json.loads(add_todo_req.content)
      nikolas_todos.append(response)

    # joe's todos

    add_todo_req = requests.post("http://localhost:5000/todo/",
                                 data={'contents': 'a todo by joe'},
                                 cookies=dict(session=user2_cookies['session']))
    response = json.loads(add_todo_req.content)
    joes_todos.append(response)

    # make sure nikola only sees two todos

    get_todos_req = requests.get("http://localhost:5000/todos/",
                                 cookies=dict(session=user1_cookies['session']))
    assert get_todos_req.status_code == 200
    response = json.loads(get_todos_req.content)
    assert len(response['items']) == 2

    # make sure joe only sees one todo

    get_todos_req = requests.get("http://localhost:5000/todos/",
                                 cookies=dict(session=user2_cookies['session']))
    assert get_todos_req.status_code == 200
    response = json.loads(get_todos_req.content)
    assert len(response['items']) == 1

    for todo in nikolas_todos:
      delete_todo_req = requests.delete(
        "http://localhost:5000/todo/{}/".format(todo['id']),
        cookies=dict(session=user1_cookies['session'])
      )

    for todo in joes_todos:
      delete_todo_req = requests.delete(
        "http://localhost:5000/todo/{}/".format(todo['id']),
        cookies=dict(session=user2_cookies['session'])
      )

if __name__ == '__main__':
  unittest.main()
