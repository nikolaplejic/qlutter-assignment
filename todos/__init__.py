from flask import Blueprint
from .views import *

todos = Blueprint('todos', __name__,
                  template_folder='templates')

routes = {
  '/todo/': {
    'name': 'create_todo',
    'fn': create_todo,
    'opts': {'methods': ['POST']}
  },
  '/todo/<int:todo_id>/complete/': {
    'name': 'complete_todo',
    'fn': mark_todo_complete,
    'opts': {'methods': ['POST']}
  },
  '/todo/<int:todo_id>/': {
    'name': 'todo',
    'fn': todo,
    'opts': {'methods': ['DELETE', 'GET']}
  },
  '/todos/': {
    'name': 'list_todos',
    'fn': list_todos,
    'opts': {'methods': ['GET']}
  },
  '/todos/complete/': {
    'name': 'list_completed_todos',
    'fn': list_todos,
    'opts': {'methods': ['GET']}
  },
  '/todos/incomplete/': {
    'name': 'list_incompleted_todos',
    'fn': list_todos,
    'opts': {'methods': ['GET']}
  },
}

for endpoint, handler in routes.items():
  todos.add_url_rule(endpoint, handler['name'], handler['fn'], **handler['opts'])
