from flask import Blueprint
from .views import *

users = Blueprint('users', __name__,
                  template_folder='templates')

routes = {
  '/': {
    'name': 'user_add',
    'fn': add_user,
    'opts': {'methods': ['POST']}
  },
  '/<int:user_id>/': {
    'name': 'user_delete',
    'fn': delete_user,
    'opts': {'methods': ['DELETE']}
  },
  '/login/': {
    'name': 'user_login',
    'fn': login,
    'opts': {'methods': ['POST']}
  },
  '/logout/': {
    'name': 'user_logout',
    'fn': logout,
    'opts': {'methods': ['GET']}
  },
  '/user_info/': {
    'name': 'user_info',
    'fn': user_info,
    'opts': {'methods': ['GET']}
  },
}

for endpoint, handler in routes.items():
  users.add_url_rule(endpoint, handler['name'], handler['fn'], **handler['opts'])
