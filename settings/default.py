import os

DEBUG = True
SCHEMA = os.path.join(os.getcwd(), 'schema.sql')
DATABASE = os.path.join(os.getcwd(), 'db.sqlite3')
SECRET_KEY = 'very secret key for development'
